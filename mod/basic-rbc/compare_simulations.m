addpath ../../matlab2tikz/src

load rbc9_simulated_data_expected;
Ke = Simulated_time_series.Capital;
Ce = Simulated_time_series.Consumption;


load rbc10_simulated_data_surprise;
Ks = Simulated_time_series.Capital;
Cs = Simulated_time_series.Consumption;

range = dates('1Q1'):dates('25Q4');

figure(1)
plot(Ke(range),'-k')
hold on
plot(Ks(range),'-r')
hold off
legend('Expected shocks','Non expected shocks')
matlab2tikz('../../img/rbc_capital_expected_versus_nonexpected_shocks.tex','width','\figurewidth');

figure(2)
plot(Ce(range),'-k')
hold on
plot(Cs(range),'-r')
hold off
legend('Expected shocks','Non expected shocks')
matlab2tikz('../../img/rbc_consumption_expected_versus_nonexpected_shocks.tex','height','\figureheight');
