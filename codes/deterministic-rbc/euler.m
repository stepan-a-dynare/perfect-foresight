function [residual, partialderivates] =  euler(variables, parameters)
% stephane.adjemian@univ-lemans.fr (2014)

c0 = variables(2);
k1 = variables(3);
c1 = variables(4);
alpha = parameters(1);
beta = parameters(2);
delta = parameters(3);

t1 = alpha*k1^(alpha-1);

residual = (c1/c0) - beta*(t1+1-delta);

if nargout>1
    partialderivates = zeros(4,1);
    partialderivates(2) = -c1/(c0*c0);
    partialderivates(3) = beta*(1-alpha)*t1/k1;
    partialderivates(4) = 1/c0;
end