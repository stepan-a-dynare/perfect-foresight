function [residual, partialderivates] =  transition(variables, parameters)
% stephane.adjemian@univ-lemans.fr (2014)

k0 = variables(1);
c0 = variables(2);
k1 = variables(3);
alpha = parameters(1);
beta = parameters(2);
delta = parameters(3);

t1 = k0^alpha;

residual = k1 - t1 - (1-delta)*k0 + c0;

if nargout>1
    partialderivates = zeros(4,1);
    partialderivates(1) = -(1-delta)-alpha*t1/k0;
    partialderivates(2) = 1;
    partialderivates(3) = 1;
end